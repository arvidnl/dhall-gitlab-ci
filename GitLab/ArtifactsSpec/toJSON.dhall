let Prelude = ../Prelude.dhall

let Map = Prelude.Map

let JSON = Prelude.JSON

let ArtifactsSpec = ../ArtifactsSpec/Type.dhall

let Optional/map = Prelude.Optional.map

let dropNones = ../utils/dropNones.dhall

let When/toJSON = ../When/toJSON.dhall

let Duration/toJSON = ../Duration/toJSON.dhall

let List/map = Prelude.List.map

in  let ArtifactsSpec/toJSON
        : ArtifactsSpec -> JSON.Type
        = \(cs : ArtifactsSpec) ->
            let reports =
                  toMap
                    { junit =
                        Optional/map Text JSON.Type JSON.string cs.reports.junit
                    , dotenv =
                        Optional/map
                          Text
                          JSON.Type
                          JSON.string
                          cs.reports.dotenv
                    }
                  : Map.Type Text (Optional JSON.Type)

            in  let reports = JSON.object (dropNones Text JSON.Type reports)

                in  let obj
                        : Map.Type Text JSON.Type
                        = toMap
                            { when = When/toJSON cs.when
                            , expire_in = Duration/toJSON cs.expire_in
                            , paths =
                                JSON.array
                                  (List/map Text JSON.Type JSON.string cs.paths)
                            , reports
                            }

                    in  JSON.object obj

    in  ArtifactsSpec/toJSON
