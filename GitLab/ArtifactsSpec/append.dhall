let ArtifactsSpec = ./Type.dhall

let mergeOptionalRight = ../utils/mergeOptionalRight.dhall

let append
    : ArtifactsSpec -> ArtifactsSpec -> ArtifactsSpec
    = \(a : ArtifactsSpec) ->
      \(b : ArtifactsSpec) ->
        { when = b.when
        , expire_in = b.expire_in
        , reports.junit
          = mergeOptionalRight Text a.reports.junit b.reports.junit
        , reports.dotenv
          = mergeOptionalRight Text a.reports.dotenv b.reports.dotenv
        , paths = a.paths # b.paths
        }

in  append
